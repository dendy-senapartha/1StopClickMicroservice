package com.microservice.auth.dao;

import com.microservice.auth.model.Role;
/*
 * Created by dendy-prtha on 20/09/2019.
 * Role dao
 */

public interface RoleDao extends Dao<Role, Long>{
}
