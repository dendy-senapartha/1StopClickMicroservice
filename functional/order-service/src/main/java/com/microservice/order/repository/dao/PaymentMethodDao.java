package com.microservice.order.repository.dao;

import com.microservice.order.model.PaymentMethod;

/*
 * Created by dendy-prtha on 11/06/2019.
 * Video Dao
 */

public interface PaymentMethodDao extends Dao<PaymentMethod, Integer> {
}
