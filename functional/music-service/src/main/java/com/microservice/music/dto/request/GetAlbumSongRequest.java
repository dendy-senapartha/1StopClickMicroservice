package com.microservice.music.dto.request;

/*
 * Created by dendy-prtha on 17/09/2019.
 * delete user request
 */

public class GetAlbumSongRequest {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
