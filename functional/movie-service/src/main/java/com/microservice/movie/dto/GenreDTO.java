package com.microservice.movie.dto;

/*
 * Created by dendy-prtha on 26/09/2019.
 * Genre DTO
 */

public class GenreDTO {

    private int id;

    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
